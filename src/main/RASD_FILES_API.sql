create or replace package RASD_FILES_API is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASD_FILES_API generated on 03.04.23 by user RASDDEV.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function Blob2Clob(b blob, c varchar2 default 'EE8MSWIN1250') return clob;
FUNCTION Clob2Blob( c IN CLOB ) RETURN BLOB;

end;
/
create or replace package body RASD_FILES_API is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASD_FILES_API generated on 28.02.24 by user RASDDEV.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
function Blob2Clob(b blob, c varchar2 default 'EE8MSWIN1250') return clob is
   type message_type is record (
        content blob,
        charset varchar2(30),
        typ varchar2(20)
   );
   v_message message_type;
   v_content clob;
   v_dest_offset number := 1;
   v_src_offset number := 1;
   v_lang_context number := dbms_lob.default_lang_ctx;
   v_warning number;
   v_x1 number;
 begin
     v_message.content := b;
     v_message.charset := c;


      dbms_lob.createtemporary(v_content, TRUE);
      v_dest_offset := 1;
      v_src_offset  := 1;
      v_lang_context := dbms_lob.default_lang_ctx;
      DBMS_LOB.CONVERTTOCLOB(v_content, v_message.content, dbms_lob.getlength  
      (v_message.content), v_dest_offset, v_src_offset,
      nls_charset_id(v_message.charset), v_lang_context, v_warning);
  
      v_x1 := instr(lower(substr(v_content,1,1000)),'encoding="');

      if v_x1 > 0 then
        v_message.charset := replace(substr(v_content,v_x1+10,instr(lower
        (substr(v_content,1,1000)),'"', v_x1+10)-v_x1-10),'-','');

        dbms_lob.createtemporary(v_content, TRUE);
        v_dest_offset := 1;
        v_src_offset  := 1;
        v_lang_context := dbms_lob.default_lang_ctx;
        DBMS_LOB.CONVERTTOCLOB(v_content, v_message.content,
        dbms_lob.getlength(v_message.content), v_dest_offset, v_src_offset,
        nls_charset_id(v_message.charset), v_lang_context, v_warning);
      end if;                         
  
   return v_content;   
 end;


FUNCTION Clob2Blob( c IN CLOB ) RETURN BLOB
-- typecasts CLOB to BLOB (binary conversion)
IS
pos PLS_INTEGER := 1;
buffer RAW( 32767 );
res BLOB;
lob_len PLS_INTEGER := DBMS_LOB.getLength( c );
BEGIN
DBMS_LOB.createTemporary( res, TRUE );
DBMS_LOB.OPEN( res, DBMS_LOB.LOB_ReadWrite );

LOOP
buffer := UTL_RAW.cast_to_raw( DBMS_LOB.SUBSTR( c, 16000, pos ) );

IF UTL_RAW.LENGTH( buffer ) > 0 THEN
DBMS_LOB.writeAppend( res, UTL_RAW.LENGTH( buffer ), buffer );
END IF;

pos := pos + 16000;
EXIT WHEN pos > lob_len;
END LOOP;

RETURN res; -- res is OPEN here
END;

     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASD_FILES_API',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
 });
        ';
        return v_out;
       end; 
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '

        ';
        return v_out;
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228091149'; 
  end;
  function this_form return varchar2 is
  begin
   return 'RASD_FILES_API';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version ); 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
declare vc varchar2(2000); begin
null;
exception when others then  null; end;
  end if;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
-- init fields
  end;
  procedure post_submit is
  begin

    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure pclear_form is
  begin
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;

  null;
  end;
  procedure pselect is
  begin


  null;
 end;
  procedure pcommit is
  begin


  null; 
  end;
  procedure formgen_js is
  begin
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
  function ShowFieldERROR return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    return true;
  end; 
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','RASD_FILES Functions')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>'); 
 
htp.prn('</head>
<body><div id="RASD_FILES_API_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASD_FILES_API_LAB','RASD_FILES Functions') ||'     </div><div id="RASD_FILES_API_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASD_FILES_API_MENU') ||'     </div>
<form name="RASD_FILES_API" method="post" action="?"><div id="RASD_FILES_API_DIV" class="rasdForm"><div id="RASD_FILES_API_HEAD" class="rasdFormHead"><input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
</div><div id="RASD_FILES_API_BODY" class="rasdFormBody"></div><div id="RASD_FILES_API_RESPONSE" class="rasdFormResponse"><div id="RASD_FILES_API_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASD_FILES_API_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASD_FILES_API_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASD_FILES_API_FOOTER" class="rasdFormFooter"></div></div></form><div id="RASD_FILES_API_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASD_FILES_API_BOTTOM',1,instr('RASD_FILES_API_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASD_FILES_API',ACTION);  
  if ACTION is null then null;
    pselect;
    poutput;
  end if;

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','RASD_FILES Functions')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head><body><div id="RASD_FILES_API_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASD_FILES_API_LAB','RASD_FILES Functions') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASD_FILES_API' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASD_FILES_API_FOOTER',1,instr('RASD_FILES_API_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>92</formid><form>RASD_FILES_API</form><version>1</version><change>28.02.2024 09/11/49</change><user>RASDDEV</user><label><![CDATA[RASD_FILES Functions]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>N</autocreaterestyn><autocreatebatchyn>N</autocreatebatchyn><addmetadatainfoyn>N</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>01.04.2023 10/41/24</change><compileyn>N</compileyn><application></application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks></blocks><fields></fields><links></links><pages></pages><triggers></triggers><elements></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASD_FILES_API_v.1.1.20240228091149.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASD_FILES_API;
/
